#-------------------------------------------------
#
# Project created by QtCreator 2018-07-25T06:48:15
#
#-------------------------------------------------

QT       += core gui bluetooth network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BT-lock
TEMPLATE = app

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        traydialog.cpp \
    bluetoothtray.cpp \
    consoledialog.cpp

HEADERS += \
        traydialog.h \
    bluetoothtray.h \
    consoledialog.h

FORMS += \
        traydialog.ui \
    bluetoothtray.ui \
    consoledialog.ui

RESOURCES = resource.qrc


win32 {
    LIBS += "C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17134.0\um\x64\User32.Lib"
    LIBS += -L"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.17134.0\um\x64"
    LIBS += -lUser32
    INCLUDEPATH += "C:\Program Files (x86)\Windows Kits\10\Include\10.0.17134.0\um"

#    LIBS += "C:\Users\develop\SmartInDigital-BT\BT-lock\3rdParty\win-bluetooth\build\Debug\win-bluetooth.lib"
    INCLUDEPATH += "C:\Users\develop\SmartInDigital-BT\BT-lock\3rdParty\win-bluetooth\include"
}
`
CONFIG(debug, debug|release){
    DESTDIR = ./build-debug
    LIBS += "C:\Users\develop\SmartInDigital-BT\BT-lock\3rdParty\win-bluetooth\build\Debug\win-bluetooth.lib"
}else{
    DESTDIR = ./build-release
    LIBS += "C:\Users\develop\SmartInDigital-BT\BT-lock\3rdParty\win-bluetooth\build\Release\win-bluetooth.lib"
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

