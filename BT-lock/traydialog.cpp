#include "traydialog.h"
#include "ui_traydialog.h"

#include <QAction>
#include <QMenu>

TrayDialog::TrayDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TrayDialog),
    hideTrayMessage(false)
{
    createActions();
    createTrayIcon();

    ui->setupUi(this);

    trayIcon->show();
}

TrayDialog::~TrayDialog()
{
    delete ui;

    delete minimizeAction;
    delete restoreAction;
    delete quitAction;

    delete trayIcon;
    delete trayIconMenu;
}

void TrayDialog::setVisible(bool visible)
{
    minimizeAction->setEnabled(visible);
    restoreAction->setEnabled(!visible);

    QDialog::setVisible(visible);
}

void TrayDialog::showTrayMessage(const QString &title, const QString &message)
{
    QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(1);
    int timeout = 1000;

    if(hideTrayMessage){ timeout = 1; }

    trayIcon->showMessage(title, message, icon, timeout);
}

void TrayDialog::reject()
{
    showTrayMessage("Systray", "This program will keep running in the system tray.. "
                               "if you should terminate program choose Quit in the context menu..");
    this->hide();
    QDialog::reject();
}
//NOTE : Subclassing from QDialog, close event not be called.
//		 so, override reject public slot
void TrayDialog::closeEvent(QEvent *ev)
{
    showTrayMessage("Systray", "This program will keep running in the system tray..\
                                if you should terminate program choose Quit in the context menu..");

    this->hide();
    ev->ignore();
}

void TrayDialog::createActions()
{
    minimizeAction = new QAction(tr("Minimize"), this);
    minimizeAction->setEnabled(false);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    restoreAction = new QAction(tr("Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    quitAction = new QAction(tr("Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void TrayDialog::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/images/heart.png"));
    trayIcon->setContextMenu(trayIconMenu);
}
