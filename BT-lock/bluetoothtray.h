﻿#ifndef BLUETOOTHTRAY_H
#define BLUETOOTHTRAY_H

#include <QDialog>
#include "traydialog.h"

#include <win-bluetooth>
#include <Bluetooth.h>
#include <bluetoothLocalDevice.h>
#include <bluetoothDeviceDiscoveryAgent.h>

#include <QTimer>
#include <QMap>
#include <QCloseEvent>

namespace Ui {
class BluetoothTray;
}

class BluetoothTray : public TrayDialog
{
    Q_OBJECT

public:
    explicit BluetoothTray(QWidget *parent = 0);
    ~BluetoothTray();

public slots:
    void addDevice(const BluetoothDeviceInfo &info);

private slots:
    void listCheck();
    void discoveryErrorHandling(BluetoothDeviceDiscoveryAgent::Error error);

protected:
    void closeEvent(QEvent *ev);

private:
private:
    Ui::BluetoothTray *ui;

    BluetoothDeviceDiscoveryAgent *discoveryAgent;
//    QBluetoothLocalDevice *localDevice;

    BluetoothDeviceInfo myDevice;

    QMap<QString, BluetoothDeviceInfo> foundedDevice;

    QTimer *timer;

};

#endif // BLUETOOTHTRAY_H
