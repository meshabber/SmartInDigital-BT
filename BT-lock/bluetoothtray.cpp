﻿#include "bluetoothtray.h"
#include "ui_bluetoothtray.h"

#include <QListWidgetItem>
#include <QDebug>

#include <windows.h>
#include <WinUser.h>

BluetoothTray::BluetoothTray(QWidget *parent) :
    TrayDialog(parent),
    ui(new Ui::BluetoothTray),
    discoveryAgent(new BluetoothDeviceDiscoveryAgent),
//    localDevice(new QBluetoothLocalDevice),
    timer(new QTimer(this))
{
    ui->setupUi(this);

    /* default actions */
    connect(ui->btn_scan, &QPushButton::clicked,
            [this](){
        qDebug() << "menual discovering..." << "\n";

        if(!discoveryAgent->isActive()) discoveryAgent->start();
    });
    connect(timer, &QTimer::timeout,
            [this](){
        if(!discoveryAgent->isActive()) discoveryAgent->start();
    });
    connect(ui->btn_clear, &QPushButton::clicked,
            [=](){
        myDevice = BluetoothDeviceInfo();
        ui->lb_mydev->setText("");
    });

    /* discovery debugging messages */
    connect(discoveryAgent, &BluetoothDeviceDiscoveryAgent::canceled,
            [this](){
        qDebug() << "discovering..stoped" << "\n";
    });
    connect(discoveryAgent, QOverload<BluetoothDeviceDiscoveryAgent::Error>::of(&BluetoothDeviceDiscoveryAgent::error),
            this,			&BluetoothTray::discoveryErrorHandling);
    // at the first discoverd devices, should not be cached.
    connect(discoveryAgent, &BluetoothDeviceDiscoveryAgent::finished,
            [this](){
        for(auto && info : discoveryAgent->discoveredDevices())
            info.setCached(false);
        if(!foundedDevice.empty()){
            this->showTrayMessage("Select Device", "Find some neared devices.."
                                                   "choose your device.");
        }
        qDebug() << "discovering..finished" << "\n";
    });

    connect(discoveryAgent, &BluetoothDeviceDiscoveryAgent::deviceDiscovered,
            this,			&BluetoothTray::addDevice);

    // select my device from discovered list
    connect(ui->listWidget, &QListWidget::itemDoubleClicked,
            [this](QListWidgetItem * item){
        QString devinfo = item->text();

        auto currentDev = foundedDevice.find(devinfo);
        myDevice = currentDev.value();
        myDevice.setCached(false);
        ui->lb_mydev->setText(myDevice.name());

        for(auto i=0;i<ui->listWidget->count();i++){
            ui->listWidget->item(i)->setBackground(QBrush(QColor(255,255,255)));
        }
        item->setBackground(QBrush(QColor(50, 150, 20)));
    });

    connect(ui->check_trayMsg, &QCheckBox::toggled,
            [=](bool toggle){
        hideTrayMessage = toggle;
    });

    /* Debug button */
    connect(ui->btn_test, &QPushButton::clicked,
            [=](){
        SendMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, 2);
        LockWorkStation();
//        ExitWindowsEx( EWX_LOGOFF, SHTDN_REASON_MINOR_OTHER );
    });

//    localDevice->setHostMode(QBluetoothLocalDevice::HostConnectable);
//    localDevice->powerOn();

    timer->start(500);
    this->showTrayMessage("First Discovering..","Be wait few minutes..");
}

BluetoothTray::~BluetoothTray()
{
    delete ui;
    delete timer;

    delete discoveryAgent;
//    delete localDevice;
}

void BluetoothTray::addDevice(const BluetoothDeviceInfo &info)
{
    QString label = QString("%1 %2")
                    .arg(info.address().toString())
                    .arg(info.name());
    qint16 rssi = info.rssi();

    qDebug() << label << " : " << rssi << "\n";

    if(0 == rssi) {
        // FIXME : [runtime error] syncronizing foundedDevice - listWidget
//        foundedDevice.remove(label);
//        auto items = ui->listWidget->findItems(label, Qt::MatchExactly);
//        for(auto && item : items){
//            ui->listWidget->removeItemWidget(item);
//        }
//        return;
    }

    auto search = foundedDevice.find(label);
    auto end = foundedDevice.end();
    if(end == search){
        foundedDevice.insert(label, info);
        ui->listWidget->addItem(new QListWidgetItem(label));
    }
    else if(search.value() == myDevice){
        myDevice.setRssi(rssi);
//        this->discoveryAgent->stop();
        return;
    }
}

void BluetoothTray::listCheck()
{
    for(auto && info : discoveryAgent->discoveredDevices())
    {
        qDebug() << "finding lists..." << "\n";
        if(info == myDevice){
            qDebug() << "find..." << "\n";
        }
    }
}

void BluetoothTray::discoveryErrorHandling(BluetoothDeviceDiscoveryAgent::Error error)
{
    qDebug() << discoveryAgent->errorString() << "\n";

    using btAgent = BluetoothDeviceDiscoveryAgent;

    switch (error) {
    case btAgent::NoError:
        break;
    case btAgent::PoweredOffError:
        break;
    case btAgent::InputOutputError:
        break;
    case btAgent::InvalidBluetoothAdapterError:
        break;
    case btAgent::UnsupportedPlatformError:
        break;
    case btAgent::UnsupportedDiscoveryMethod:
        break;
    case btAgent::UnknownError:
        break;
    default:
        break;
    }
}

void BluetoothTray::closeEvent(QEvent *ev)
{
    this->showTrayMessage("Systray", "This program will keep running in the system tray.."
                                     "if you should terminate program, choose <b>Quit</b> in the context menu..");

    this->hide();
    ev->ignore();
}
