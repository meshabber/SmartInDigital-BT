#ifndef TRAYDIALOG_H
#define TRAYDIALOG_H

#include <QDialog>
#include <QSystemTrayIcon>

class QAction;
class QMenu;

namespace Ui {
class TrayDialog;
}

class TrayDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TrayDialog(QWidget *parent = 0);
    ~TrayDialog();

    void setVisible(bool visible) override;

public slots:
    void showTrayMessage(const QString &title="Title", const QString &message="SomeThing..");
    void reject() override;
protected:
    void closeEvent(QEvent *ev);

private slots:
    void createActions();
    void createTrayIcon();

private:

private:
    Ui::TrayDialog *ui;

    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

protected:
    bool hideTrayMessage;
};

#endif // TRAYDIALOG_H
