# Bluetooth 개요
Android Bluetooth를 개발하기전에 Bluetooth 통신에대한 약간의 이해가 필요합니다. 
Bluetooth Process를 간단히 설명하자면 주변에 연결하려는 디바이스를 찾고 찾은 기기중 연결할 기기에 인증을 받은 후 그리고 나서 통신을 합니다.

이 내용은 크게 Discovery,Pairing,Connection,Streaming 4가지 STEP으로 나눌수 있습니다.

### 1.Discovery
주변 디바이스 검색 합니다
  - Discovery는 주변에 블루투스와 연결 가능한 디바이스를 검색하는것 입니다.
    검색되는 디바이스는 기기가 검색되는 시점마다 BroadcastReceiver에 디바이스의 정보가 리턴됩니다. 리턴된 Device의 정보는 Device연결 정보와 특징 등을  담고 있습니다. 그중 눈여겨 볼 내용은 디바이스주소 입니다. 일종에 IP주소와 같은 개념으로 디바이스와 연결을 할수 있는 정보 입니다.

### 2.Pairing
디비이스와 인증합니다.
  - pairing은 일종에 기기에 대한 인증절차라고 보시면 됩니다.
	한번 pairing이 이루어지게 되면 삭제하기 전까지 또다시 paring할 필요는 없습니다. pairing은 디바이스주소를 기반으로 이루어지게 되고 이때 연결시 패스워드를 요구할수 있으며 pairing의 결과는 BroadcastReceiver로 리턴됩니다. 이후 pairing이 성공되면 기기와 Connection에 대한 권한을 얻게되어 Connection을 할수 있게됩니다.

### 3.Connection
디바이스와 연결 합니다.
  - Connection 단계에서는 Bluetooth device간 통신을 위해 UUID가 필요한데 이는 해당 프로토콜을 의미합니다.
UUID는 Universally Unique IDentifier로 SDP에서 서비스의 종류를 구분하기 위한 128비트 포맷의 표준화 된 문자열 ID입니다.
안드로이드 플랫폼의 단말기끼리는 어떤 UUID를 사용하여도 되지만 특정 프로토콜의 device에 접근하기 위해서는 Bluetooth 표준 프로토콜 UUID를 사용하여야 합니다. 다시말해 connection을 하려면 해당 디바이스에서 사용하고 있는 UUID를 알아야만 디바이스와 연결할수 있습니다.
Connection의 방식으로 Server와Client로 기존 Socket통신 방식과 동일하며 해당 서비스에 맞게 선택하여 구현할수 있습니다. 간단하게 Server와Client 설명하자면 Client는 디바이스로 연결을 요청하여 Connection하는 것이며 Server는 디바이스에 연결 요청을 받아 Connection 해주는 방식입니다. Connection이 연결된 결과는 BroadcastReceiver로 리턴받을수 있습니다.

### 4.Streaming
디바이스와 데이터 통신을 합니다.
  - streaming은 실제 데이터를 주고받는 일이며 디바이스의 프로토콜에 맞게 스펙을 해석하고 구현하는 것을 말합니다. 

# Bluetooth 라이브러리 사용
Bluetooth 라이브러리에서는 Server/Client 대한 멀티 connection을 제공합니다.
모든 conntection은 Controller에 통합관리하며 비지니스 로직에 대한 정의는
Listener형태로 제공되고 있습니다.
- BtController Class 개요
  Bluetooth의 통신 전반에 걸칠 Control 클래스 입니다.
  BtController는 디바이스의 Discovery, paring, connection을 구동하고 관리할수 있습니다.
  또한 결과인 BroadcastReceiver를 Listener로 연결해주며 등록할수 있는 Listener는 3가지가 있습니다.  
  1.BtSearchListener  
    Discovery에 관련된 디바이스 검색(onSearch,endSearch)과 pairing(onPairing) 결과 Listener 입니다.

  2.BtServerListener
    Bluetooth Server에 관련된 결과 Linster로 서버시작(onStart),서버종료(onDisonnect),
    접속(onConnect),접속종료(onStop)가 있습니다.
    onStart과 onDisonnect는 Server의 Binding의 startup과 shutdown을 말하며 
    onConnect과onStop은 클라이언트 접속과 해제를 뜻합니다.	

  2.BtClientListener
    Bluetooth Client에 관련된 결과 Linster로 접속(onConnect),접속종료(onStop)가 있습니다.
    디바이스와 접속(onConnect)과 접속종료(onStop)에 대한 결과를 처리 할수 있습니다.

### Reading the Bluetooth Connection RSSI

RSSI can be a bit weird to read, but essentially a higher number  means a better connection, and a lower number means a worse connection.  Note however that the numbers are negative, so that may read opposite to  what you’d expect. For example, a connection of -45 is significantly  stronger and better than a connection of -100, which is weaker and more  likely to have issues. The rough guidelines below may help read the  connection, though the precise signal you get is going to vary on other  factors we’ll discuss below:

- -40 to -55 is a very strong connection
- -70 and above represents a good connection
- -100 and below represents a bad connection
- -110 and below is almost unusable